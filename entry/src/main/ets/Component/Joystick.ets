import { curves } from '@kit.ArkUI';

@Preview
@Component
export struct Joystick {
  onMove?: (x: number, y: number) => void;
  @State centerX: number = 100
  @State centerY: number = 100
  // The start position of the joystick
  @State positionX: number = this.centerX
  @State positionY: number = this.centerY
  // raduis of joystick's base
  @State radiusMax: number = 100
  // radius of joystick
  @State radiusMin: number = 40
  @State sin: number = 0
  @State cos: number = 0
  @State joyStickType: number = 0 //0：x,y自动回弹   1: x自动回弹，y不回弹
  @Prop @Watch('onLockChange')isLock: boolean = false
  //intervalId: number = 0

  build() {
    Row() {
      Circle({ width: this.radiusMax * 2, height: this.radiusMax * 2 })
        .fill('#11000000')
        .position({ x: this.centerX - this.radiusMax, y: this.centerY - this.radiusMax })

      Circle({ width: this.radiusMin * 2, height: this.radiusMin * 2 })
        .fill('#ef3f3f3f')
        .position({ x: this.positionX - this.radiusMin, y: this.positionY - this.radiusMin })

      Circle({ width: this.radiusMax * 2, height: this.radiusMax * 2 })
        .fill('#00000000')
        .position({ x: this.centerX - this.radiusMax, y: this.centerY - this.radiusMax })
        .onTouch(this.handleTouchEvent.bind(this))
    }
    .width(200)
    .height(200)
    .justifyContent(FlexAlign.Center)
  }

  getTouchNum(touches: TouchObject[]) {
    let minId = -1;
    let minDis = 0x3f3f3f3f;
    for (let i = 0; i < touches.length; i++) {
      let currentDis = Math.sqrt(touches[i].x * touches[i].x + touches[i].y * touches[i].y)
      if(currentDis < minDis)
      {
        minDis = currentDis
        minId = i
      }
    }
    return minId
  }

  getDistance(x: number, y: number) {
    let distance = Math.sqrt(x * x + y * y)
    return Math.min(distance, this.radiusMax)
  }

  setMovePosition(event: TouchEvent) {
    //console.log(event.touches.length + 'len')
    let currentId = this.getTouchNum(event.touches)
    let x = event.touches[currentId].x
    let y = event.touches[currentId].y
    //console.log(x+'  '+y)
    let vx = x - this.radiusMax
    let vy = -(y - this.radiusMax)
    let angle = Math.atan2(vy, vx)
    let distance = this.getDistance(vx, vy)
    this.sin = Math.sin(angle)
    this.cos = Math.cos(angle)

    this.positionX = this.centerX + distance * this.cos
    this.positionY = this.centerY - distance * this.sin
    if(this.onMove)
      this.onMove(distance * this.cos, distance * this.sin)
    //console.log(distance * this.cos + '  ' + distance * this.sin)
  }

  joyStickBehavior() {
    switch (this.joyStickType) {
      case 0:
        if (this.onMove) this.onMove(0, 0)
        animateTo({
          curve: curves.springMotion()
        }, () => {
          this.positionX = this.centerX
          this.positionY = this.centerY
        })
        break
      case 1:
        if (this.onMove) this.onMove(0, this.centerY - this.positionY)
        animateTo({
          curve: curves.springMotion()
        }, () => {
          this.positionX = this.centerX
        })
        break
      default:
        if (this.onMove) this.onMove(0, 0)
        animateTo({
          curve: curves.springMotion()
        }, () => {
          this.positionX = this.centerX
          this.positionY = this.centerY
        })
        break
    }
  }

  onLockChange() {
    if(this.isLock)
    {
      console.log('locked')
    } else {
      this.joyStickBehavior()
      console.log('unlocked')
    }
  }

  handleTouchEvent(event: TouchEvent) {
    switch (event.type) {
      case TouchType.Down:
        // this.intervalId = setInterval(() => {
        //
        // }, 40)
        break
      case TouchType.Move:
        if (!this.isLock)
          this.setMovePosition(event)
        break
      case TouchType.Up:
        //clearInterval(this.intervalId)
        if (!this.isLock) {
          this.joyStickBehavior()
        }
        //this.speed = 0
        break
    }
  }
}